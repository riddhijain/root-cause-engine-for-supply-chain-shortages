
<!DOCTYPE html>
<?php
session_start();
//$usernamelogged=$_SESSION['usernamelogged'];
include('connect.php');
$sql = mysqli_query($db_con,"SELECT issue_no,part,supplier,root_cause,action FROM details WHERE resolved=1");
?>
<html>
<head>
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <!------ Include the above in your HEAD tag ---------->
  <style>
  .project-tab {
    padding: 10%;
    margin-top: -8%;
}
.project-tab #tabs{
    background: #007b5e;
    color: #eee;
}
.project-tab #tabs h6.section-title{
    color: #eee;
}
.project-tab #tabs .nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
    color: #0062cc;
    background-color: transparent;
    border-color: transparent transparent #f3f3f3;
    border-bottom: 3px solid !important;
    font-size: 16px;
    font-weight: bold;
}
.project-tab .nav-link {
    border: 1px solid transparent;
    border-top-left-radius: .25rem;
    border-top-right-radius: .25rem;
    color: #0062cc;
    font-size: 16px;
    font-weight: 600;
}
.project-tab .nav-link:hover {
    border: none;
}
.project-tab thead{
    background: #f3f3f3;
    color: #333;
}
.project-tab a{
    text-decoration: none;
    color: #333;
    font-weight: 600;
}
</style>
<link rel="stylesheet" type="text/css" href="final_form.css">
</head>
<body>

  <section id="tabs" class="project-tab">
              <div class="container">
                  <div class="row">
                      <div class="col-md-12">

                          <div class="tab-content" id="nav-tabContent">
                              <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                  <table class="table" cellspacing="0">
                                      <thead>
                                          <tr>
                                              <th>Issue No</th>
                                              <th>Part</th>
                                              <th>Supplier</th>
                                              <th>Root cause</th>
                                              <th>Action</th>
                                          </tr>
                                      </thead>
                                      <tbody>
                                        <?php
                                      while($array=mysqli_fetch_array($sql,MYSQLI_ASSOC))
                                      {
                                      ?>
                                          <tr>
                                    					<td><?php echo $array['issue_no'];?></td>
                                              <td><?php echo $array['part'];?></td>
                                    						<td><?php echo $array['supplier'];?></td>
                                              <td><?php echo $array['root_cause'];?></td>
                                              <td><?php echo $array['action'];?></td>
                                          </tr>
                                            <?php } ?>
                                      </tbody>
                                  </table>
                              </div>
                              <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                  <table class="table" cellspacing="0">
                                      <thead>
                                          <tr>
                                              <th>Project Name</th>
                                              <th>Employer</th>
                                              <th>Time</th>
                                          </tr>
                                      </thead>
                                      <tbody>
                                          <tr>
                                              <td><a href="#">Work 1</a></td>
                                              <td>Doe</td>
                                              <td>john@example.com</td>
                                          </tr>
                                          <tr>
                                              <td><a href="#">Work 2</a></td>
                                              <td>Moe</td>
                                              <td>mary@example.com</td>
                                          </tr>
                                          <tr>
                                              <td><a href="#">Work 3</a></td>
                                              <td>Dooley</td>
                                              <td>july@example.com</td>
                                          </tr>
                                      </tbody>
                                  </table>
                              </div>
                              <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                                  <table class="table" cellspacing="0">
                                      <thead>
                                          <tr>
                                              <th>Contest Name</th>
                                              <th>Date</th>
                                              <th>Award Position</th>
                                          </tr>
                                      </thead>
                                      <tbody>
                                          <tr>
                                              <td><a href="#">Work 1</a></td>
                                              <td>Doe</td>
                                              <td>john@example.com</td>
                                          </tr>
                                          <tr>
                                              <td><a href="#">Work 2</a></td>
                                              <td>Moe</td>
                                              <td>mary@example.com</td>
                                          </tr>
                                          <tr>
                                              <td><a href="#">Work 3</a></td>
                                              <td>Dooley</td>
                                              <td>july@example.com</td>
                                          </tr>
                                      </tbody>
                                  </table>
                              </div>
                          </div>
                      </div>
                      <form  action="index.php" method="post">
                        <button type="submit" style="margin-left:200px;">Send</button>
                      </form>
                  </div>
              </div>

          </section>

        </body>
</html>
