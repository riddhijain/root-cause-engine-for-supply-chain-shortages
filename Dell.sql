-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 07, 2019 at 07:57 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.1.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Dell`
--

-- --------------------------------------------------------

--
-- Table structure for table `details`
--

CREATE TABLE `details` (
  `issue_no` varchar(50) NOT NULL,
  `part` varchar(50) NOT NULL,
  `site` varchar(50) NOT NULL,
  `team` varchar(50) DEFAULT NULL,
  `supplier` varchar(50) NOT NULL,
  `order_date` date NOT NULL,
  `amount` int(11) NOT NULL,
  `root_cause` varchar(50) DEFAULT NULL,
  `Action` varchar(50) NOT NULL,
  `resolved` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `details`
--

INSERT INTO `details` (`issue_no`, `part`, `site`, `team`, `supplier`, `order_date`, `amount`, `root_cause`, `Action`, `resolved`) VALUES
('#1001', 'Hard Drive - DX45C3', 'Factory', 'A', 'SanDisk', '2019-08-01', 1205, 'Inaccurate_forecast', 'Contact HUB to check account location', 1),
('#1002', 'IC-Pcd5101', 'ODM', 'A', 'Cryptron', '2019-08-04', 925, 'Staff_problems', '', 0),
('#1002', 'DVD Driver-ts1633', 'Factory', 'A', 'Xfurbish', '2019-08-02', 647, 'Staff_problems', '', 0),
('#1004', 'Processor-i7', 'ODM', 'A', 'Intel', '2019-07-30', 815, 'Inaccurate_forecast', 'Contact HUB to check account location', 1),
('#1005', 'DR720 Drum-unit', 'Factory', 'A', 'Brother OEM', '2019-08-05', 342, NULL, '', 0),
('#1005', 'DR720 Drum-unit', 'Factory', 'A', 'Brother OEM', '2019-08-05', 342, NULL, '', 0),
('#1006', 'RM-1-1088 Tray', 'Factory', 'A', 'HP', '2019-08-05', 200, NULL, '', 0),
('#1007', 'GV N710D3-2GL', 'Factory', 'A', 'GeForce', '2019-08-03', 120, NULL, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `mapping`
--

CREATE TABLE `mapping` (
  `root_cause` varchar(30) NOT NULL,
  `action` varchar(100) NOT NULL,
  `sub_root_cause` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mapping`
--

INSERT INTO `mapping` (`root_cause`, `action`, `sub_root_cause`) VALUES
('Transit_issues', 'Contact supplier and expedite ', 'Held at HUB'),
('Transit_issues', 'Contact the Transport co. and re-place the order', 'Damaged in transit'),
('Supplier_issue', 'Change supplier', 'Materian constraint on supplier side'),
('Supplier_issue', 'Buyer to work with the supplier to increase the DSI level.', 'Supplier didn\'t maintain a healthy DSI'),
('Transportation_delay', 'Work with carrier and correct contacts to recover ASAP', 'Trucking or air delay'),
('Transportation_delay', 'Contact network carrier and loop in Kirk', 'Shipment held at customs'),
('Bad_purchase', 'Replace and re-order', 'Wrong order placed'),
('Bad_purchase', 'Replace the order', 'Parts not supported'),
('Bad_purchase', 'Contact correct buyer & expedite process to set PO', 'PO not available against MRP'),
('Qualitty_issues', 'Reboxing/Inspection', 'Carrier damaged the material'),
('Qualitty_issues', 'Work with extended team to provide most recent update ', 'Quality issue at manufacturing plant'),
('Inaccurate_forecast', 'Contact HUB to check account location', 'Site unable to  see HUB inventory'),
('Inaccurate_forecast', 'Place another order', 'Sudden increase in demand'),
('Staff_problems', 'Train the staff', 'Unskilled staff'),
('Staff_problems', 'Motivate the staff and monitor their activities', 'Poor co-ordination'),
('Staff_problems', 'Hire MUJ students(Team 22)', 'Staff shortage'),
('Payment_issues', 'Contact the bank', 'Transaction related problems'),
('Payment_issues', 'Pay the supplier ASAP', 'Payment not done on time');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `sr_no` bigint(20) UNSIGNED NOT NULL,
  `q1` int(11) NOT NULL,
  `q2` int(11) NOT NULL,
  `q3` int(11) NOT NULL,
  `q4` int(11) NOT NULL,
  `q5` int(11) NOT NULL,
  `q6` int(11) NOT NULL,
  `q7` int(11) NOT NULL,
  `q8` int(11) NOT NULL,
  `root cause` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`sr_no`, `q1`, `q2`, `q3`, `q4`, `q5`, `q6`, `q7`, `q8`, `root cause`) VALUES
(70, 1, 1, 1, 1, 1, 1, 1, 1, 'Inaccurate_forecast'),
(71, 1, 0, 1, 0, 1, 0, 1, 0, 'Staff_Shortage'),
(72, 0, 1, 0, 1, 0, 1, 0, 1, 'Financial_Problem'),
(73, 1, 1, 1, 1, 1, 1, 1, 1, ''),
(74, 1, 1, 1, 1, 1, 1, 1, 1, ''),
(75, 1, 1, 1, 1, 1, 1, 1, 1, ''),
(76, 1, 1, 1, 1, 1, 1, 1, 1, ''),
(77, 1, 1, 1, 1, 1, 1, 1, 1, ''),
(78, 1, 1, 1, 1, 1, 1, 1, 1, 'Inaccurate_forecast'),
(79, 1, 1, 1, 1, 1, 1, 1, 1, 'Inaccurate_forecast'),
(80, 1, 1, 1, 1, 1, 1, 1, 1, 'Inaccurate_forecast');

-- --------------------------------------------------------

--
-- Table structure for table `testquestions`
--

CREATE TABLE `testquestions` (
  `sr_no` bigint(20) UNSIGNED NOT NULL,
  `q1` int(11) NOT NULL,
  `q2` int(11) NOT NULL,
  `q3` int(11) NOT NULL,
  `q4` int(11) NOT NULL,
  `q5` int(11) NOT NULL,
  `q6` int(11) NOT NULL,
  `q7` int(11) NOT NULL,
  `q8` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testquestions`
--

INSERT INTO `testquestions` (`sr_no`, `q1`, `q2`, `q3`, `q4`, `q5`, `q6`, `q7`, `q8`) VALUES
(1, 1, 0, 1, 0, 1, 0, 1, 0),
(2, 1, 1, 1, 1, 1, 1, 1, 1),
(3, 1, 1, 1, 1, 1, 1, 1, 1),
(4, 1, 1, 1, 1, 1, 1, 1, 1),
(5, 1, 0, 1, 0, 1, 0, 1, 0),
(6, 1, 1, 1, 1, 1, 1, 1, 1),
(7, 1, 1, 1, 1, 1, 1, 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD UNIQUE KEY `sr_no` (`sr_no`);

--
-- Indexes for table `testquestions`
--
ALTER TABLE `testquestions`
  ADD UNIQUE KEY `sr_no` (`sr_no`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `sr_no` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT for table `testquestions`
--
ALTER TABLE `testquestions`
  MODIFY `sr_no` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
