<!DOCTYPE HTML>
<!--
	Verti by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<?php
session_start();
//$usernamelogged=$_SESSION['usernamelogged'];
include('connect.php');
$sql = mysqli_query($db_con,"SELECT issue_no,part,site,supplier,root_cause,action FROM details WHERE resolved=1");
?>
<html>
<head>
		<title>Dell Hack2Hire</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<body class="is-preload homepage">
		<div id="page-wrapper">

			<!-- Header -->
				<div id="header-wrapper">
					<header id="header" class="container">

						<!-- Logo -->
							<div id="logo">
								<h1><a href="index.html"><img src="images/dell2.png"></a></h1>
								<span>Hack2Hire</span>
							</div>

						<!-- Nav -->
							<nav id="nav">
								<h1 id="gsm">General Supply Management</h1>
							</nav>

					</header>
				</div>

			<!-- Main -->
				<div id="main-wrapper">
					<div class="container">
						<div class="row gtr-200">
							<div class="col-3 col-12-medium">
								<div id="sidebar">
									<!-- Sidebar -->

									<div id="sidebar1">

										<section>

											<a href="view-issues.php"><h4>VIEW ISSUES</h4></a><hr>
											<a href="generate.php"><h4>RESOLVED ISSUES</h4></a>
										</section>
</div>
								</div>
							</div>
							<div class="col-8 col-12-medium imp-medium">
								<div id="content">

									<!-- Content -->
										<section>

											<div id="user" class="tbl-header">
    <table cellpadding="0" cellspacing="0" border="0">
      <thead>
        <tr>
          <th>Issue no</th>
          <th>Part</th>
          <th>Site</th>
          <th>supplier</th>
          <th>Root cause</th>
					<th>Action</th>
					<th></th>
        </tr>
      </thead>
    </table>
  </div>

  <div  class="tbl-content">
    <table cellpadding="0" cellspacing="0" border="0">
      <tbody>
				<?php
			while($array=mysqli_fetch_array($sql,MYSQLI_ASSOC))
			{
			?>
        <tr>
					<td id="gtd1"><?php echo $array['issue_no'];?></td>
          <td id="gtd2"><?php echo $array['part'];?></td>
					<td id="gtd3"><?php echo $array['site'];?></td>
						<td id="gtd4"><?php echo $array['supplier'];?></td>
          <td id="gtd5"><?php echo $array['root_cause'];?></td>
          <td id="gtd6"><?php echo $array['action'];?></td>
                </tr>
  <?php } ?>
      </tbody>
    </table>

  </div>


										</section>
										<form action="view-rep.php">
										<button type="submit" name="submit" style="margin-left:250px";>Generate Report</button>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>


		<!-- Scripts -->

			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>
