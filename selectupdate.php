<?php
//session_start();
//$usernamelogged=$_SESSION['usernamelogged'];
include('connect.php');
$sql = mysqli_query($db_con,"SELECT * FROM questions");
?>
<html>
	<head>
		<title>Dell Hack2Hire</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<body class="is-preload homepage">
		<div id="page-wrapper">
				</div>
			<!-- Main -->

								<!-- Content -->
									<div id="content">
										<section class="last">
											<h3 style="margin-top:20px;">Select the entries to be updated</h3>
											<div id="user" class="tbl-header">

    <table cellpadding="0" cellspacing="0" border="0">
      <thead>
        <tr>
          <th>Select</th>
          <th>Q1</th>
          <th>Q2</th>
          <th>Q3</th>
          <th>Q4</th>
          <th>Q5</th>
          <th>Q6</th>
          <th>Q7</th>
          <th>Q8</th>
          <th>Root Cause</th>
        </tr>
      </thead>
    </table>
  </div>

  <form method="post" action="dbupdate.php">

  <div  class="tbl-content">
    <table cellpadding="0" cellspacing="0" border="0">
      <tbody>
				<?php
			while($array=mysqli_fetch_array($sql,MYSQLI_ASSOC))
			{
			?>
        <tr>
          <td><?php echo '<input id="chkbx"  type="checkbox" name = "checkbox[]" value = " '.$array['sr_no'].'  ">'; ?>
          <td><?php echo $array['q1']?'Yes':'No';?></td>
          <td><?php echo $array['q2']?'Yes':'No';?></td>
          <td><?php echo $array['q3']?'Yes':'No';?></td>
          <td><?php echo $array['q4']?'Yes':'No';?></td>
          <td><?php echo $array['q5']?'Yes':'No';?></td>
          <td><?php echo $array['q6']?'Yes':'No';?></td>
          <td><?php echo $array['q7']?'Yes':'No';?></td>
          <td><?php echo $array['q8']?'Yes':'No';?></td>
          <td><?php echo $array['root cause'];?></td>

      </tr>
        <?php } ?>


      </tbody>
    </table>
    <button name="select" class="button icon " type="submit"><i class="" aria-hidden="true"></i>&nbsp;&nbsp;Select</button>

  </div>
										</section>
									</div>

</form>

		<!-- Scripts -->

			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>
