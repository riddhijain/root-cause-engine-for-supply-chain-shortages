import pickle
import sys
import pandas as pd
from sklearn import preprocessing
le = preprocessing.LabelEncoder()
def main(val):
    val1=list(map(int,val[0].strip('[]').split(',')))
    print(val1)
    m=pickle.load(open("om.sav",'rb'))

    a=pd.Series(val1)
    df1 = pd.DataFrame([list(a)],  columns =  ["Q1", "Q2", "Q3","Q4","Q5","Q6","Q7","Q8"])
    prediction=m.predict(df1)
    #a=list(le.inverse_transform([prediction]))
    return str(prediction)

if __name__ == "__main__":
	print(main(sys.argv[1:])) 
