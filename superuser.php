<!DOCTYPE HTML>
<!--
	Verti by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<?php
//session_start();
//$usernamelogged=$_SESSION['usernamelogged'];
include('connect.php');
$sql = mysqli_query($db_con,"SELECT * FROM questions");
?>
<html>
	<head>
		<title>Dell Hack2Hire</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<body class="is-preload homepage">
    <div class="container">
		<div id="page-wrapper">


				</div>
			<!-- Main -->

								<!-- Content -->
									<div id="content">
										<section class="last">
											<h2 style="margin-top:20px;">Existing database</h2>
                      <a href="final_form.php" class="button icon "><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;Add</a>
                       <a href="selectupdate.php" class="button icon "><i class="fa fa-spinner" aria-hidden="true"></i>&nbsp;&nbsp;Update</a>
                        <a href="dbdelete.php" class="button icon "><i class="fa fa-trash" aria-hidden="true"></i>&nbsp;&nbsp;Delete</a>
                        <br>
                        <br>
											<div id="user" class="tbl-header">

    <table cellpadding="0" cellspacing="0" border="0">
      <thead>
        <tr>
					<th class="try">Q1</th>
          <th class="try">Q2</th>
          <th class="try">Q3</th>
          <th class="try">Q4</th>
          <th class="try">Q5</th>
          <th class="try">Q6</th>
          <th class="try">Q7</th>
          <th class="try">Q8</th>
          <th class="try">Root Cause</th>
        </tr>
      </thead>
    </table>
  </div>

  <div  class="tbl-content">
    <table cellpadding="0" cellspacing="0" border="0">
      <tbody>
				<?php
			while($array=mysqli_fetch_array($sql,MYSQLI_ASSOC))
			{
			?>
        <tr>
          <td id="trytd1"><?php echo $array['q1']?'Yes':'No';?></td>
          <td id="trytd2"><?php echo $array['q2']?'Yes':'No';?></td>
          <td id="trytd3"><?php echo $array['q3']?'Yes':'No';?></td>
          <td id="trytd4"><?php echo $array['q4']?'Yes':'No';?></td>
          <td id="trytd5"><?php echo $array['q5']?'Yes':'No';?></td>
					<td id="trytd6"><?php echo $array['q6']?'Yes':'No';?></td>
          <td id="trytd7"><?php echo $array['q7']?'Yes':'No';?></td>
					<td id="trytd8"><?php echo $array['q8']?'Yes':'No';?></td>
          <td id="trytd9"><?php echo $array['root cause'];?></td>

      </tr>
        <?php } ?>


      </tbody>
    </table>

  </div>
										</section>
									</div>
</div>

		<!-- Scripts -->

			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>
