# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import pickle 
import pandas as pd
import sys
from sklearn import preprocessing
le = preprocessing.LabelEncoder()
def main(val):
    
    val=list(map(int,val[0].strip('[]').split(',')))
    """
    Attributes:None
    This method builds the tree from the dataset provided
    Return value:
        Returns the tree object
    """
    
    
    file_location="part.xlsx"
    df=pd.read_excel(file_location)
    df=df[['Q1','Q2','Q3','Q4','Q5','Q6','Q7','Q8','Root cause']]
    
    """
    Transformation of data
    
    """
    
    
    
    for col_header in df.columns.values:     
        le.fit(list(df[col_header]))
        df[col_header]=le.transform(list(df[col_header]))
        
    
    """
    Writing a new dataframe
    
    """
    
    from pandas import ExcelWriter
    
    writer = ExcelWriter('sampledata1.xlsx')
    df.to_excel(writer,'Sheet1')
    writer.save()
    
    """
    Start with splitting data
    
    """
    from sklearn.model_selection import train_test_split
    X=df.loc[:,'Q1':'Q8']
    Y=df["Root cause"]
    X_train, X_test, y_train, y_test = train_test_split( 
              X, Y, test_size = 0.3, random_state = 100)
    from sklearn.tree import DecisionTreeClassifier 
    clf_gini = DecisionTreeClassifier(criterion = "gini", 
                random_state = 100,max_depth=9, min_samples_leaf=1) 
      
    # Performing training 
    clf_gini.fit(X_train, y_train) 
    
    """
    Random Forest
    """
    from sklearn.ensemble import RandomForestClassifier
    clf=RandomForestClassifier(n_estimators=100)
    clf.fit(X_train,y_train)
  
    
    
    
    
    from sklearn.metrics import accuracy_score
    y_pred=clf_gini.predict(X_test)
    #print("Accuracy of decision tree is: ",accuracy_score(y_test, y_pred))
    y_pred=clf.predict(X_test)
    #print("Accuracy of Random Forest is:",accuracy_score(y_test,y_pred))
    from sklearn.metrics import confusion_matrix
    results = confusion_matrix(y_test, y_pred)
    #print(results)
    

#tree_obj=pickle.dump(Tree_Builder(),open('om.sav','wb'))
    

    #print(val)
    a=pd.Series(val)
    
    df1 = pd.DataFrame([list(a)],  columns =  ["Q1", "Q2", "Q3","Q4","Q5","Q6","Q7","Q8"])
    
    prediction=clf.predict(df1)
    
    a=list(le.inverse_transform([prediction]))
    
    return str(a[0]) 
   
if __name__ == "__main__":
	print(main(sys.argv[1:])) 
    
    
    
